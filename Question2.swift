#!/usr/bin/swift
//
//  Question2.swift
//  
//
//  Created by Mongkol Munglarkthum on 21/05/2017.
//
//

import Foundation

protocol Connect : Equatable {
  var source: Island { get set }
  var destination: Island { get set }
  var date: Int { get set }
}

protocol Uniqe : Equatable {
  var name: Int { get set }
}

func ==<T : Connect>(lhs:T , rhs:T) -> Bool {
  return lhs.source == rhs.source && lhs.destination == rhs.destination && lhs.date == rhs.date
}

func ==<T : Uniqe>(lhs:T , rhs:T) -> Bool {
  return lhs.name == rhs.name
}

struct Island: Uniqe {
  var name: Int
}

struct Bridge: Connect {
  var source: Island
  var destination: Island
  var date: Int
  
  init(source: Island, destination: Island, date: Int) {
    self.source = source
    self.destination = destination
    self.date = date
  }
}

var allBridges: [Bridge] = []
var allIslands: [Island] = []

// get another Island that connect with Bridge
func getDestination(from source: Island, bridge: Bridge) -> Island {
  if bridge.source == source {
    return bridge.destination
  } else {
    return bridge.source
  }
}

// array for keep data when search all path
var allpaths: [[Bridge]] = []
var visits: [Island] = []
var bridges: [Bridge] = []

// search all path between Souuce and destination
func search(source: Island, destination: Island) {
  visits.append(source)
  if source == destination {
    allpaths.append(bridges)
    bridges.removeLast()
    visits.removeLast()
    return
  } else {
    let connectedBridges = allBridges.filter{
      return ($0.source == source || $0.destination == source) && !(visits.contains($0.source) && visits.contains($0.destination))
    }
    if connectedBridges.count > 0 {
      for bridge in connectedBridges {
        let des = getDestination(from: source, bridge: bridge)
        if !visits.contains(des) {
          bridges.append(bridge)
          search(source: des, destination: destination)
        }
      }
      if bridges.count > 0 {
        bridges.removeLast()
        visits.removeLast()
      }
      return
    } else {
      if bridges.count > 0 {
        bridges.removeLast()
        visits.removeLast()
      }
      return
    }
  }
}

// find newest path from All path
func foundNewestPaht(from paths:[[Bridge]]) -> [Bridge] {
  var index = 0
  var newest = 0
  var newestIndex = 0
  for path in paths {
    if let newlestBridge = path.max(by: { $0.0.date < $0.1.date }) {
      if newlestBridge.date > newest {
        newest = newlestBridge.date
        newestIndex = index
      }
    }
    index += 1
  }
  
  return paths[newestIndex]
}

// remove oldest bridge in path
func removeOldestBridge(bridges: [Bridge]) -> Bridge?{
  let oldestBridge = bridges.max { $0.0.date > $0.1.date}
  if let oldestBridgeDate = oldestBridge?.date {
    allBridges = allBridges.filter{
      return $0.date != oldestBridgeDate }
    return oldestBridge
  }
  return nil
}

// run app
print("Please enter 2 numbers separated with a space 'N Q'.((1 ≦ N ≦ 50),(1 ≦ Q ≦ 500))")

var numberOfIsland = 0
var numberOfDay = 0
repeat {
  if let inputNQ = readLine() {
    let nqStrings = inputNQ.components(separatedBy: " ")
    if nqStrings.count == 2,
      let n = Int(nqStrings[0]) ,
      let q = Int(nqStrings[1]),
      1 <= n, n <= 50, 1 <= q, q <= 500 {
      numberOfIsland = n
      numberOfDay = q
      for index in 1...numberOfIsland {
        allIslands.append(Island(name:index))
      }
      
    } else {
      print("Please input correct format")
    }
  }
} while numberOfIsland == 0 && numberOfDay == 0

var i = 0
print("Please enter command 'build A B' or 'check A B'(1≦A,B≦N and A≠B). \(numberOfDay) times")
while(i < numberOfDay) {
  print("Input \(i+1): ")
  if let inputCommand = readLine() {
    let commandArray = inputCommand.components(separatedBy: " ")
    if commandArray.count == 3,
      let a = Int(commandArray[1]),
      let b = Int(commandArray[2]),
      (commandArray[0] == "build" || commandArray[0] == "check"),
      a >= 1, a <= numberOfIsland, b >= 1, b <= numberOfIsland, a != b {
      let command = commandArray[0]
      if command == "build" {
        if let source = allIslands.filter({$0.name == a}).first,
          let destination = allIslands.filter({$0.name == b}).first {
          let bridge = Bridge(source: source, destination: destination, date: i+1)
          allBridges.append(bridge)
          i += 1
        }
      } else if command == "check" {
        if let source = allIslands.filter({$0.name == a}).first,
          let destination = allIslands.filter({$0.name == b}).first {
          allpaths.removeAll()
          visits.removeAll()
          bridges.removeAll()
          search(source: source, destination: destination)
          if allpaths.count > 0 {
            let newestPath = foundNewestPaht(from: allpaths)
            let oldestBridge = removeOldestBridge(bridges: newestPath)
            print("YES \(oldestBridge!.date)")
          } else {
            print("NO")
          }
          i += 1
        }
      }
      
    } else {
      print("Please enter correct format")
    }
  } else {
    print("Please enter correct format")
  }
  print("--------")
}



