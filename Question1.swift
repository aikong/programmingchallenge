#!/usr/bin/swift
//
//  Question1.swift
//  
//
//  Created by Mongkol Munglarkthum on 14/05/2017.
//
//

import Foundation

func reOrder(numberSet set: [(Int, Int)]) -> String {
  let original = [1,2,3,4,5,6,7,8]
  var numbers = [1,2,3,4,5,6,7,8]
  let n = set[0].0
  let k = set[0].1
  
  // find round when can loop to same orignal number list
  var round = 0
  repeat {
    round += 1
    for index in 1...n {
      let a = set[index].0
      let b = set[index].1
      (numbers[a - 1], numbers[b - 1]) = (numbers[b - 1], numbers[a - 1])
    }
    
  } while original != numbers
  
  let numberOfRepeat = k % round
  if numberOfRepeat == 0 {
    print(numbers)
    return numbers.map { String($0) }.joined(separator: " ")
  }
  
  for _ in 1...numberOfRepeat {
    for index in 1...n {
      let a = set[index].0
      let b = set[index].1
      (numbers[a - 1], numbers[b - 1]) = (numbers[b - 1], numbers[a - 1])
    }
  }
  
  return numbers.map { String($0) }.joined(separator: " ")
}

print("Please enter 2 numbers separated with a space 'N K'.((1 ≦ N ≦ 50),(1 ≦ K ≦ 10^9))")

var set: [(Int, Int)] = []
var numberOfOperations = 0
repeat {
  if let inputNK = readLine() {
    let nkStrings = inputNK.components(separatedBy: " ")
    if nkStrings.count == 2,
      let n = Int(nkStrings[0]) ,
      let k = Int(nkStrings[1]),
      1 <= n, n <= 50, 1 <= k, k <= 1000000000 {
      numberOfOperations = n
      set.append((n, k))
    } else {
      print("Please input correct format")
    }
  }
} while set.count < 1

var i = 0
print("Please enter 2 numbers separated with a space 'A B'(1≦Ai, Bi≦8, Ai≠Bi). \(numberOfOperations) times")

while(i < numberOfOperations) {
  print("Input \(i+1): ")
  if let inputAB = readLine() {
    let abStrings = inputAB.components(separatedBy: " ")
    if abStrings.count == 2,
      let a = Int(abStrings[0]),
      let b = Int(abStrings[1]),
      a >= 1, a <= 8, b >= 1 , b <= 8, a != b {
      set.append((a,b))
      i += 1
      
    } else {
      print("Please enter correct format")
    }
  } else {
    print("Please enter correct format")
  }
  print("--------")
}

print("Output:")
print(reOrder(numberSet: set))
